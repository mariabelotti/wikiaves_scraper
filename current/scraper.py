# -*- coding: utf-8 -*-

'''
Created on 18 de ago de 2018
@author: tositstill
'''

import csv
import os
import time
import requests
import sys
import json
import datetime
import numpy as np

from bs4 import BeautifulSoup

start = time.time()


species_list = ["Pygochelidon cyanoleuca", "Pygochelidon melanoleuca", "Alopochelidon fucata", "Atticora fasciata", 
                "Atticora tibialis", "Stelgidopteryx ruficollis", "Progne tapera", "Progne subis",
                "Progne chalybea", "Progne elegans", "Tachycineta albiventer", "Tachycineta leucorrhoa",
                "Tachycineta leucopyga", "Riparia riparia", "Hirundo rustica", "Petrochelidon pyrrhonota"]


def get_species_id(query_species):

    with open('species_database.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=';')
        line_count = 0
        for row in csv_reader:
            if row[0] == query_species:
                print(row[1])
                return row[1]

# def deep_dive(link):

#     record_page = requests.get(link)
#     soup = BeautifulSoup(record_page.text, 'lxml')
#     print(link)

#     t = soup.find('div', class_='wa-lista-detalhes').contents

#     string_t = str(t[6])
#     start_pt = string_t.find("/label>") + 7
#     assunto = string_t[start_pt:-6]


#     string_t = str(t[7])

#     if string_t.find("Ação") == -1:
#         start_pt = string_t.find("/label>") + 7
#         sexo = string_t[start_pt:-6]

#         string_t = str(t[8])
#         start_pt = string_t.find("/label>") + 7
#         idade = string_t[start_pt:-6]

#         acao = "Vazio"
#         print(assunto, acao, sexo, idade)
#         return [assunto, acao, sexo, idade] 

    
#     start_pt = string_t.find("/label>") + 7
#     acao = string_t[start_pt:-6]

#     string_t = str(t[8])
#     start_pt = string_t.find("/label>") + 7
#     sexo = string_t[start_pt:-6]

#     string_t = str(t[9])
#     start_pt = string_t.find("/label>") + 7
#     idade = string_t[start_pt:-6]

#     print(assunto, acao, sexo, idade)
#     return [assunto, acao, sexo, idade]


def last_page_finder(species_id):
    record_list_url = "https://www.wikiaves.com.br/getRegistrosJSON.php?tm=f&t=s&s=" + str(species_id) + "&o=mp&o=mp&p=10000"
    record_list_page = requests.get(record_list_url)
    record_list_raw = json.loads(record_list_page.text)

    total = int(record_list_raw["registros"]["total"])
    pages = int(np.floor(total/20) + 1)
    print(total, pages)
    return(total, pages)

# def image_download(link, record_code):
#
#         record_page = requests.get(link)
#         soup = BeautifulSoup(record_page.text,'lxml')
#
#         image_raw = str(soup.find(attrs={"id": "imgFoto"}))
#         start_pt = image_raw.find("src") + 5
#         end_pt = image_raw.find("jpg") + 3
#         image_link = "http://www.wikiaves.com.br/" + image_raw[start_pt:end_pt]
#         if not os.path.isdir('database/'):
#             os.makedirs('database/')
#         image_path = "database/" + record_code + ".jpg"
#
#         u = requests.get(image_link)
#         with open(image_path,"wb") as img:
#             for chunk in u.iter_content(chunk_size=128):
#                 img.write(chunk)
#         print("Acabei de baixar a imagem do registro " + str(record_code))
#         print("--------------------------------------------------------------------------------")

def main_scraper(species_list):

    page_count = 0

    current_date = time.strftime("%Y_%m_%d")
    filename = current_date + ".csv"

    with open(filename, 'a') as csvfile:
        w = csv.writer(csvfile, delimiter=";")

        for species in species_list:
            species_id = get_species_id(species)
            print("Começando a espécie " + species)
            print(species_id)
            total, pages = last_page_finder(species_id)
            i = 1
            for page_count in range(1,pages + 1):

                print("Comecei a página " + str(page_count) + " de " + str(pages))

                record_list_url = "https://www.wikiaves.com.br/getRegistrosJSON.php?tm=f&t=s&s=" + str(species_id) + "&o=mp&o=mp&p=" + str(page_count)
                record_list_page = requests.get(record_list_url)
                record_list_raw = json.loads(record_list_page.text)

                for record_count in range(1,21):
                    try:
                        record_code = str(record_list_raw["registros"]["itens"][str(record_count)]["id"])
                        print("Registro " + str(i) + " de " + str(total))
                        print(record_code)

                        link = "https://www.wikiaves.com.br/" + str(record_code)

                        author = record_list_raw["registros"]["itens"][str(record_count)]["autor"]

                        data = record_list_raw["registros"]["itens"][str(record_count)]["data"]

                        local = record_list_raw["registros"]["itens"][str(record_count)]["local"]
                        city_code = record_list_raw["registros"]["itens"][str(record_count)]["idMunicipio"]

                        barra = local.find("/")

                        cidade = local[:barra]

                        estado = local[barra+1:]

                        # #image_download(link, record_code)
                        #record_data = deep_dive(link)

                        row = ["wikiaves", record_code, species_id, species, cidade, estado, link, author, data, city_code]
                        w.writerow(row)
                        print("--------------------------------------------------------------------------------")
                        i = i + 1

                    except:
                        row = ["wikiaves", "failed", species_id, species, "-", "-", "-", "-", "-", "-"]
                        w.writerow(row)

main_scraper(species_list)

end = time.time()
runtime = end - start
print(runtime)
