# -*- coding: utf-8 -*-

'''
Enferrujado - Lathrotriccus euleri
Para o trabalho do Denis (Observatório de Aves)
maio de 2019
'''
#Remember to update the number of pages to scrap through. This is not automatic.

import csv
import os
import time
import requests
import sys
import json
import datetime

start = time.time()

query_species = "enferrujado" 
query_state = ""

number_list = [] #Grava todos os números de registro, sem o "WA" inicial, para acessar a página do registro depois.
data_list = [] # Grava as datas dos registros --- TO DO: separa dia, mês e ano para faciliar a classificação
author_list = []
city_list = [] # Grava a cidade do registro
state_list = []
record_list = [] # Cria as linhas do arquivo CSV, com cada uma das informações recolhidas por registro
link_list = [] # Lista de links para cada registro.
species_list = []
name_list = []

#
# def get_species_id(query_species):
#
#     with open('species_database.csv') as csv_file:
#         csv_reader = csv.reader(csv_file, delimiter=';')
#         line_count = 0
#         for row in csv_reader:
#             if row[0] == query_species:
#                 print(row[1])
#                 return row[1]


# def last_page_finder(payload):
#     record_list_url = "https://www.wikiaves.com.br/midias.php?tm=f&t=b&p=1000000"
#     record_list_page = requests.post(record_list_url, data=payload)
#     record_list_page.encoding = 'iso-8996'
#
#     soup = BeautifulSoup(record_list_page.text, 'lxml')
#
#     links_raw = soup.find(attrs={"class": "paginacao"})
#     if len(links_raw) == 0:
# 	       number = 1
#
#     try:
#         links_raw = links_raw.find_all('a')
#         if len(links_raw) == 0:
#             number = 0
#
#         else:
#             if len(links_raw) < 12:
#                 number = len(links_raw) - 1
#
#             else:
#                 links_raw = str(links_raw[11])
#
#                 start_pt = links_raw.find("p=") + 2
#                 end_pt = links_raw.find("\"", start_pt)
#
#                 number = int(links_raw[start_pt:end_pt])
#
#         print(number)
#
#     except (AttributeError):
#         number = 0
#
#     return number



# def image_download(link, record_code):
#
#         record_page = requests.get(link)
#         soup = BeautifulSoup(record_page.text,'lxml')
#
#         image_raw = str(soup.find(attrs={"id": "imgFoto"}))
#         start_pt = image_raw.find("src") + 5
#         end_pt = image_raw.find("jpg") + 3
#         image_link = "http://www.wikiaves.com.br/" + image_raw[start_pt:end_pt]
#         if not os.path.isdir('database/'):
#             os.makedirs('database/')
#         image_path = "database/" + record_code + ".jpg"
#
#         u = requests.get(image_link)
#         with open(image_path,"wb") as img:
#             for chunk in u.iter_content(chunk_size=128):
#                 img.write(chunk)
#         print("Acabei de baixar a imagem do registro " + str(record_code))
#         print("--------------------------------------------------------------------------------")

def main_scraper():

    page_count = 0

    current_date = time.strftime("%d_%m_%Y")
    filename = current_date + "_" + query_species + ".csv"

    with open(filename, 'a') as csvfile:
        w = csv.writer(csvfile, delimiter=",")

        for page_count in range(1,168):

            print("Comecei a página " + str(page_count))
            time.sleep(5)

            record_list_url = "https://www.wikiaves.com.br/getRegistrosJSON.php?tm=f&t=s&s=11280&o=mp&o=mp&p=" + str(page_count)

            record_list_page = requests.get(record_list_url)
            record_list_raw = json.loads(record_list_page.text)

            for record_count in range(1,21):
                record_code = record_list_raw["registros"]["itens"][str(record_count)]["id"]
                number_list.append(record_code)
                print(record_code)

                link = "https://www.wikiaves.com.br/" + str(record_code)

                author = record_list_raw["registros"]["itens"][str(record_count)]["autor"]
                author_list.append(author)
                print(author)

                data = record_list_raw["registros"]["itens"][str(record_count)]["data"]
                data_list.append(data)
                print(data)

                local = record_list_raw["registros"]["itens"][str(record_count)]["local"]
                city_code = record_list_raw["registros"]["itens"][str(record_count)]["idMunicipio"]

                print(local)
                barra = local.find("/")

                cidade = local[:barra]
                print(cidade)

                estado = local[barra+1:]
                print(estado)

                city_list.append(cidade)
                state_list.append(estado)

                # #image_download(link, record_code)

                row = [record_code, link, cidade, estado, city_code, author, data]
                w.writerow(row)

                print("--------------------------------------------------------------------------------")

                page_count = page_count + 1

main_scraper()

end = time.time()
runtime = end - start
print(runtime)
