# -*- coding: utf-8 -*-

'''
Created on 15 de ago de 2017
@author: tositstill
'''

from bs4 import BeautifulSoup
import csv
import os
import time
import requests
import sys

start = time.time()

query_species_list = ["Megarynchus pitangua", "Tyrannus melancholicus", "Myiozetetes similis", "Empidonomus varius", "Lathrotriccus euleri"]
query_state = "SP"

number_list = [] #Grava todos os números de registro, sem o "WA" inicial, para acessar a página do registro depois.
data_dia_list = [] # Grava as datas dos registros --- TO DO: separa dia, mês e ano para faciliar a classificação
data_mes_list = []
data_ano_list = []
city_list = [] # Grava a cidade do registro
state_list = []
record_list = [] # Cria as linhas do arquivo CSV, com cada uma das informações recolhidas por registro
link_list = [] # Lista de links para cada registro.
species_list = []
name_list = []


def get_species_id(query_species):

    with open('species_database.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=';')
        line_count = 0
        for row in csv_reader:
            if row[0] == query_species:
                print(row[1])
                return row[1]


def last_page_finder(payload):

    record_list_url = "http://www.wikiaves.com.br/midias.php?tm=f&t=b&p=1000000"
    record_list_page = requests.post(record_list_url, data=payload)
    record_list_page.encoding = 'iso-8996'

    soup = BeautifulSoup(record_list_page.text, 'lxml')

    links_raw = soup.find(attrs={"class": "paginacao"})
    links_raw = links_raw.find_all('a')
    links_raw = str(links_raw[11])

    start_pt = links_raw.find("p=") + 2
    end_pt = links_raw.find("\"", start_pt)

    number = int(links_raw[start_pt:end_pt]) * 10
    print(number)
    return number


def image_download(link, record_code):

        record_page = requests.get(link)
        soup = BeautifulSoup(record_page.text,'lxml')

        image_raw = str(soup.find(attrs={"id": "imgFoto"}))
        start_pt = image_raw.find("src") + 5
        end_pt = image_raw.find("jpg") + 3
        image_link = "http://www.wikiaves.com.br/" + image_raw[start_pt:end_pt]
        if not os.path.isdir('database/'):
            os.makedirs('database/')
        image_path = "database/" + record_code + ".jpg"

        u = requests.get(image_link)
        with open(image_path,"wb") as img:
            for chunk in u.iter_content(chunk_size=128):
                img.write(chunk)
        print("Acabei de baixar a imagem do registro " + str(record_code))
        print("--------------------------------------------------------------------------------")


def main_scraper():
    count = 0
    for species in query_species_list:

        query_id = get_species_id(species)
        query_species = species

        payload = {"especie": query_species, "especie_hidden": query_id, "idadeF": "", "tipoAssuntoAve": "",
                   "tipoAssuntoAlimento": "", "cidade": "", "cidade_hidden": "", "estado": query_state, "camera": "",
                   "dataInicioRecebida": "", "dataFimRecebida": "", "dataInicioRegistro": "", "dataFimRegistro": ""}

        #page_number = last_page_finder(payload)
        page_number = 400
        with open("record_index_tiranideos.csv", 'a') as csvfile:
            w = csv.writer(csvfile, delimiter=";")

            for page_count in range(1,page_number):

                print("Comecei a página " + str(page_count) + " de " + str(page_number) + ".")

                record_list_url = "https://www.wikiaves.com.br/midias.php?t=b&p=" + str(page_count)

                record_list_page = requests.post(record_list_url, data=payload)
                record_list_page.encoding = 'iso-8996'
                print(record_list_page.status_code)

                soup = BeautifulSoup(record_list_page.text, 'lxml')
                record_list_raw = soup.find_all(attrs={"class": "dados"})

                for item in record_list_raw:

                    item_str = str(item)

                    start_pt = item_str.find("WA")
                    end_pt = item_str.find("</div>")-2
                    record_code = item_str[start_pt:end_pt]
                    record_number = item_str[start_pt+2:end_pt]
                    number_list.append(record_code)

                    print("Iniciando processamento do registro " + str(record_code))
                    start_pt = item_str.find("Feita em")+17
                    end_pt = item_str.find("<br/>",start_pt)
                    record_data = item_str[start_pt:end_pt]

                    start_pt = item_str.find("<i>") + 3
                    end_pt = item_str.find("</i>")
                    species_list.append(item_str[start_pt:end_pt])

                    start_pt = item_str.find("<b>") + 3
                    end_pt = item_str.find("</b>")
                    name_list.append(item_str[start_pt:end_pt])

                    dia = record_data[:2]
                    mes = record_data[3:5]
                    ano = record_data[6:]

                    data_dia_list.append(dia)
                    data_mes_list.append(mes)
                    data_ano_list.append(ano)

                    link = "http://www.wikiaves.com.br/" + record_number
                    link_list.append(link)

                    record_page = requests.get(link)
                    record_page.encoding = 'iso-8996'
                    print(record_page.status_code)

                    soup = BeautifulSoup(record_page.text,'lxml')
                    local_raw = soup.find(attrs={"class": "tipoLocal"})
                    local = local_raw.get_text()
                    #start_pt = local.find(":") + 2

                    barra = local.find("/")

                    #cidade = local[start_pt:barra]
                    cidade = local[:barra]
                    print(cidade)
                    estado = local[barra+1:]
                    #print(estado)
                    city_list.append(cidade)
                    state_list.append(estado)

                    image_download(link, record_code)

                    row = [record_code, query_species, link, cidade, estado, dia, mes, ano]
                    w.writerow(row)

                print("--------------------------------------------------------------------------------")

            page_count = page_count + 1

        count = count + 1



main_scraper()

end = time.time()
runtime = end - start
print(runtime)

