# wikiaves_scraper

This project contains several files that were used to scra the Wikiaves database to obtain record metadata and, sometimes, low resolution photos of the birds. 

There is a lot of work to do on the documentation of the scripts. I am currently working on it. 

Refer to the Wiki to find out more and read our **very serious disclaimer**: https://gitlab.com/maria.belotti.99/wikiaves_scraper/wikis/Home